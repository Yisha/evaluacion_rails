class CreatePos < ActiveRecord::Migration[5.0]
  def change
    create_table :pos do |t|
      t.integer :shop_id
      t.integer :number

      t.timestamps
    end
  end
end
