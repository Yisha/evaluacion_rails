class CreateProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles do |t|
      t.string :name
      t.integer :crear
      t.integer :editar
      t.integer :leer
      t.integer :eliminar
      t.integer :total
      t.datetime :created_at
      t.datetime :updated_at

      t.timestamps
    end
  end
end
