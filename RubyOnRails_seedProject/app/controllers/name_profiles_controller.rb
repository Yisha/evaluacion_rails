class NameProfilesController < ApplicationController
  before_action :set_name_profile, only: [:show, :edit, :update, :destroy]




  # GET /name_profiles
  # GET /name_profiles.json
  def index

    @name_profiles = NameProfile.all

  end

  # GET /name_profiles/1


  # GET /name_profiles/1.json
  def show


  end


  # GET /name_profiles/new
  def new

    @name_profile = NameProfile.new
  end

  # GET /name_profiles/1/edit
  def edit

  end

  # POST /name_profiles
  # POST /name_profiles.json
  def create
    @name_profile = NameProfile.new(name_profile_params)

    respond_to do |format|

      if NameProfile.where("name like ?", @name_profile.name).exists?
        format.html {
          @error = 'Name Profile Name like'
          render action: :new
        }

      else

        if @name_profile.save

          @profile = Profile.new
          @profile.name = params[:name1]
          @profile.crear = params[:crear1]
          @profile.editar = params[:editar1]
          @profile.leer = params[:leer1]
          @profile.eliminar = params[:eliminar1]
          @profile.total = params[:crear1].to_i + params[:editar1].to_i + params[:leer1].to_i + params[:eliminar1].to_i
          @profile.nameProfile_id = @name_profile.id
          @profile.save


          @profile = Profile.new
          @profile.name = params[:name2]
          @profile.crear = params[:crear2]
          @profile.editar = params[:editar2]
          @profile.leer = params[:leer2]
          @profile.eliminar = params[:eliminar2]
          @profile.total = params[:crear2].to_i + params[:editar2].to_i + params[:leer2].to_i + params[:eliminar2].to_i
          @profile.nameProfile_id = @name_profile.id
          @profile.save

          @profile = Profile.new
          @profile.name = params[:name3]
          @profile.crear = params[:crear3]
          @profile.editar = params[:editar3]
          @profile.leer = params[:leer3]
          @profile.eliminar = params[:eliminar3]
          @profile.total = params[:crear3].to_i + params[:editar3].to_i + params[:leer3].to_i + params[:eliminar3].to_i
          @profile.nameProfile_id = @name_profile.id
          @profile.save

          @profile = Profile.new
          @profile.name = params[:name4]
          @profile.crear = params[:crear4]
          @profile.editar = params[:editar4]
          @profile.leer = params[:leer4]
          @profile.eliminar = params[:eliminar4]
          @profile.total = params[:crear4].to_i + params[:editar4].to_i + params[:leer4].to_i + params[:eliminar4].to_i
          @profile.nameProfile_id = @name_profile.id
          @profile.save

          @profile = Profile.new
          @profile.name = params[:name5]
          @profile.crear = params[:crear5]
          @profile.editar = params[:editar5]
          @profile.leer = params[:leer5]
          @profile.eliminar = params[:eliminar5]
          @profile.total = params[:crear5].to_i + params[:editar5].to_i + params[:leer5].to_i + params[:eliminar5].to_i
          @profile.nameProfile_id = @name_profile.id
          @profile.save

          @profile = Profile.new
          @profile.name = params[:name6]
          @profile.crear = params[:crear6]
          @profile.editar = params[:editar6]
          @profile.leer = params[:leer6]
          @profile.eliminar = params[:eliminar6]
          @profile.total = params[:crear6].to_i + params[:editar6].to_i + params[:leer6].to_i + params[:eliminar6].to_i
          @profile.nameProfile_id = @name_profile.id
          @profile.save

          if @name_profile.save
            format.html { redirect_to @name_profile, notice: 'Name profile was successfully created.' }
            format.json { render :show, status: :created, location: @name_profile }
          else
            format.html { render :new }
            format.json { render json: @name_profile.errors, status: :unprocessable_entity }

          end
        end
      end
    end
  end

  # PATCH/PUT /name_profiles/1
  # PATCH/PUT /name_profiles/1.json
  def update
    respond_to do |format|
      if @name_profile.update(name_profile_params)

        @name1 = params[:name1].to_s


        @profile1 = Profile.where("name = '"+ @name1 +"' and nameProfile_id = " + params[:id])
        @profile1.each do |f|

          f.update_attribute(:crear, params[:crear1])
          f.update_attribute(:editar, params[:editar1])
          f.update_attribute(:leer, params[:leer1])
          f.update_attribute(:eliminar, params[:eliminar1])
          @total1 = params[:crear1].to_i + params[:editar1].to_i + params[:leer1].to_i + params[:eliminar1].to_i
          f.update_attribute(:total, @total1.to_i)
        end

        @name2 = params[:name2].to_s
        @profile2 = Profile.where("name = '"+ @name2 +"' and nameProfile_id = " + params[:id])
        @profile2.each do |f|

          f.update_attribute(:crear, params[:crear2])
          f.update_attribute(:editar, params[:editar2])
          f.update_attribute(:leer, params[:leer2])
          f.update_attribute(:eliminar, params[:eliminar2])
          @total2 = params[:crear2].to_i + params[:editar2].to_i + params[:leer2].to_i + params[:eliminar2].to_i
          f.update_attribute(:total, @total2.to_i)
        end
        @name3 = params[:name3].to_s
        @profile3 = Profile.where("name = '"+ @name3 +"' and nameProfile_id = " + params[:id])
        @profile3.each do |f|

          f.update_attribute(:crear, params[:crear3])
          f.update_attribute(:editar, params[:editar3])
          f.update_attribute(:leer, params[:leer3])
          f.update_attribute(:eliminar, params[:eliminar3])
          @total3 = params[:crear3].to_i + params[:editar3].to_i + params[:leer3].to_i + params[:eliminar3].to_i
          f.update_attribute(:total, @total3.to_i)
        end

        @name4 = params[:name4].to_s
        @profile4 = Profile.where("name = '"+ @name4 +"' and nameProfile_id = " + params[:id])
        @profile4.each do |f|

          f.update_attribute(:crear, params[:crear4])
          f.update_attribute(:editar, params[:editar4])
          f.update_attribute(:leer, params[:leer4])
          f.update_attribute(:eliminar, params[:eliminar4])
          @total4 = params[:crear4].to_i + params[:editar4].to_i + params[:leer4].to_i + params[:eliminar4].to_i
          f.update_attribute(:total, @total4.to_i)
        end

        @name5 = params[:name5].to_s
        @profile5 = Profile.where("name = '"+ @name5 +"' and nameProfile_id = " + params[:id])
        @profile5.each do |f|

          f.update_attribute(:crear, params[:crear5])
          f.update_attribute(:editar, params[:editar5])
          f.update_attribute(:leer, params[:leer5])
          f.update_attribute(:eliminar, params[:eliminar5])
          @total5 = params[:crear4].to_i + params[:editar5].to_i + params[:leer5].to_i + params[:eliminar5].to_i
          f.update_attribute(:total, @total5.to_i)
        end

        @name6 = params[:name6].to_s
        @profile6 = Profile.where("name = '"+ @name6 +"' and nameProfile_id = " + params[:id])
        @profile6.each do |f|

          f.update_attribute(:crear, params[:crear6])
          f.update_attribute(:editar, params[:editar6])
          f.update_attribute(:leer, params[:leer6])
          f.update_attribute(:eliminar, params[:eliminar6])
          @total6 = params[:crear6].to_i + params[:editar6].to_i + params[:leer6].to_i + params[:eliminar6].to_i
          f.update_attribute(:total, @total6.to_i)
        end
        format.html { redirect_to @name_profile, notice: 'Name profile was successfully updated.' }
        format.json { render :show, status: :ok, location: @name_profile }

      else
        format.html { render :edit }
        format.json { render json: @name_profile.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /name_profiles/1
  # DELETE /name_profiles/1.json
  def destroy
    @name_profile.destroy
    respond_to do |format|
      format.html { redirect_to name_profiles_url, notice: 'Name profile was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_name_profile
    @name_profile = NameProfile.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def name_profile_params
    params.require(:name_profile).permit(:name)
  end
end
