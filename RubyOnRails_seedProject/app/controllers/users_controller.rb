class UsersController < ApplicationController

  before_filter :authenticate_user!

  @@user = 'Users'

  def index
    @users = User.all
  end

  def show

  end


  def update
    @user = User.find(params[:id])
    if params[:user][:nameProfile_id].present?
      if @user.update_attributes(secure_params).present?
        redirect_to users_path, :notice => "User updated."
        ChangePerfil.permisos_otorgados(@user).deliver
      else
        redirect_to users_path, :alert => "Unable to update user."
      end
    else
      redirect_to users_path, :alert => "You have not selected a profile."
    end
  end

  def permissions
    @user = current_user
    @admin = User.where("nameProfile_id = 1")
    @admin.each do |f|
      @email = f.email
      ChangePerfil.mandar_permisos(@user, @email).deliver
    end
    redirect_to home_index_path, :notice => "We got an email with the granted permissions."
  end

  def destroy
    user = User.find(params[:id])
    user.destroy
    redirect_to users_path, :notice => "User deleted."
  end

  private

  def secure_params
    params.require(:user).permit(:nameProfile_id)
  end


end
