class NameProfile < ApplicationRecord

  has_many :profiles , dependent: :destroy, foreign_key: "nameProfile_id"
  has_many :users

end
