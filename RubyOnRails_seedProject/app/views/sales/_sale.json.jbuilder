json.extract! sale, :id, :pos_id, :ticket, :amount, :created_at, :updated_at
json.url sale_url(sale, format: :json)
